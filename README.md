# Frontend Mentor - Blogr landing page solution

This is a solution to the [Blogr landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/blogr-landing-page-EX2RLAApP). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)


### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size.
- See hover states for all interactive elements on the page.

### Built with

- Semantic HTML5 markup
- SASS custom properties
- Flexbox
- [Vanilla JavaScript](http://vanilla-js.com//) - For hamburger menu
- [Styled Components](https://sass-lang.com/) - For styles


### What I learned

In this challenge I learned basic Sass organization and custom styling and how to use a lot of properties that were new to me.
I also learned how to upload a repository in gitlab.

### Continued development

I would like to focus and refine more on styling with Sass and interaction using JavaScript.

### Useful resources

- [Sass](https://www.youtube.com/watch?v=nu5mdN2JIwM) - This helped me with Sass.
- [Git tutorial](https://www.youtube.com/watch?v=8JJ101D3knE&t=2074s) - Great video. I'd recommend it to anyone still learning this concept.

## Author

- name - [Orianis Castro]()
- Ayudinga - [ocastro@ayudinga.org]
- email - [orianis2530@gmail.com]


